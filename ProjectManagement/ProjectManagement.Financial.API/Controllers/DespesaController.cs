﻿using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Core.Services;
using ProjectManagement.Tables;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManagement.Financial.API.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class DespesasController : ControllerBase
    {
        private readonly IDespesaService _despesaService;
        public DespesasController(IDespesaService despesaService)
        {
            this._despesaService = despesaService;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Tables.Despesa>>> GetAllExpensives()
        {
            try
            {
                var expensives = await _despesaService.GetAllWithSubRubrica();
                return Ok(expensives);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Despesa>> GetById(int id)
        {
            try
            {
                var expensives = await _despesaService.GetDespesaById(id);
                return Ok(expensives);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        public async Task<IActionResult> CreateDespesa(Despesa newDespesa)
        {
            try
            {
                var despesa = await _despesaService.CreateDespesa(newDespesa);

                await _despesaService.CreateDespesa(despesa);
                return Ok(despesa);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Despesa despesa)
        {
            if (despesa == null)
            {
                return BadRequest();
            }

            Despesa putObject = await _despesaService.GetDespesaById(despesa.Id_despesa);

            putObject.Descricao = despesa.Descricao;

            var response = _despesaService.UpdateDespesa(putObject);

            if (response == null)
            {
                return NotFound();
            }
            return NoContent();
        }

        [HttpDelete("{Id_despesa}")]
        public async Task<IActionResult> DeleteDespesa(int Id_despesa)
        {
            if (Id_despesa == 0)
                return BadRequest();
            var despesa = await _despesaService.GetDespesaById(Id_despesa);
            if (despesa == null)
                return NotFound();

            await _despesaService.DeleteDespesa(despesa);
            return NoContent();
        }
    }
}