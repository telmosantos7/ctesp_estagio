﻿using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Core.Services;
using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectManagement.Financial.API.Controllers
{

    [Route("v1/[controller]")]
    [ApiController]
    public class UtilizadorController : ControllerBase
    {
        private readonly IUtilizadorService _utilizadorService;
        public UtilizadorController(IUtilizadorService utilizadorService)
        {
            this._utilizadorService = utilizadorService;
        }


        [HttpGet]
        public async Task<ActionResult<Utilizador>> GetAll()
        {
            try
            {
                var users = await _utilizadorService.GetUtilizadores();
                return Ok(users);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Utilizador>> GetById(int id)
        {
            try
            {
                var expensives = await _utilizadorService.GetUtilizadorById(id);
                return Ok(expensives);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
        [HttpPost]
        public async Task<IActionResult> CreateUtilizador(Utilizador newUtilizador)
        {
            try
            {
                var utilizador = await _utilizadorService.CreateUtilizador(newUtilizador);

                await _utilizadorService.CreateUtilizador(utilizador);
                return Ok(utilizador);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> Update(Utilizador utilizador)
        {
            if (utilizador == null)
            {
                return BadRequest();
            }

            Utilizador putObject = await _utilizadorService.GetUtilizadorById(utilizador.Id_utilizador);

            putObject.Cargo = utilizador.Cargo;

            var response = _utilizadorService.UpdateUtilizador(putObject);

            if (response == null)
            {
                return NotFound();
            }
            return NoContent();
        }

        [HttpDelete("{Id_utilizador}")]
        public async Task<IActionResult> DeleteUtilizador(int Id_utilizador)
        {
            if (Id_utilizador == 0)
                return BadRequest();
            var utilizador = await _utilizadorService.GetUtilizadorById(Id_utilizador);
            if (utilizador == null)
                return NotFound();

            await _utilizadorService.DeleteUtilizador(utilizador);
            return NoContent();
        }
    }
}

