﻿using ProjectManagement.Tables;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManagement.RH.Services
{
    public interface IAtividadeService
    {
        Task<IEnumerable<Atividade>> GetAllWithEsforcoAsync();
        Task<IEnumerable<Atividade>> GetAllWithTimeSheetAsync();
        Task<Atividade> GetEsforcoById(int Id);
        Task<Atividade> GetTimeSheetById(int Id);
        Task<IEnumerable<Atividade>> GetEsforcoById_esforco(int Id_esforco);
        Task<IEnumerable<Atividade>> GetTimeSheetById_timeSheet(int Id_timeSheet);
        Task<Atividade> CreateAtividade(Atividade newAtividade);
        Task UpdateAtividade(Atividade atividadeToBeUpdated, Atividade atividade);
        Task DeleteAtividade(Atividade atividade);
    }
}