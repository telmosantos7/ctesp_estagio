﻿using ProjectManagement.Data.Tables;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManagement.RH.Services
{
    public interface IRecursosHumanosService
    {
        Task<IEnumerable<RecursosHumanos>> GetAllWithTimeSheet();
        Task<RecursosHumanos> GetTimeSheetById(int Id);
        Task<IEnumerable<RecursosHumanos>> GetRecursosHumanosById_timeSheet(int Id_timeSheet);
        Task<RecursosHumanos> CreateRecursosHumanos(RecursosHumanos newRecursoshumanos);
        Task UpdateRecursosHumanos(RecursosHumanos recursoshumanosToBeUpdated, RecursosHumanos recursoshumanos);
        Task DeleteRecursosHumanos(RecursosHumanos recursoshumanos);
    }
}