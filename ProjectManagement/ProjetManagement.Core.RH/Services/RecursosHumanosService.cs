﻿using ProjectManagement.Core.RH.Repositories;
using ProjectManagement.Data.Tables;
using ProjectManagement.RH.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.RH.Services
{
   public class RecursosHumanosService : IRecursosHumanosService
    {

        private readonly IUnitOfWork _unitOfWork;
        public RecursosHumanosService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<RecursosHumanos> CreateRecursosHumanos(RecursosHumanos newRecursosHumanos)
        {
            await _unitOfWork.RecursosHumanos.AddAsync(newRecursosHumanos);
            await _unitOfWork.CommitAsync();
            return newRecursosHumanos;
        }

        public async Task DeleteRecursosHumanos(RecursosHumanos recursoshumanos)
        {
            _unitOfWork.RecursosHumanos.Remove(recursoshumanos);
            await _unitOfWork.CommitAsync();
        }

        public async Task<IEnumerable<RecursosHumanos>> GetAllWithTimeSheet()
        {
            return await _unitOfWork.RecursosHumanos
                .GetAllWithTimeSheetAsync();
        }

        public async Task<RecursosHumanos> GetTimeSheetById(int Id)
        {
            return await _unitOfWork.RecursosHumanos
                .GetWithTimeSheetByIdAsync(Id);
        }

        public async Task<IEnumerable<RecursosHumanos>> GetRecursosHumanosById_timeSheet(int Id_timeSheet)
        {
            return await _unitOfWork.RecursosHumanos
                .GetAllWithTimeSheetById_Id_timeSheetAsync(Id_timeSheet);
        }

        public async Task UpdateRecursosHumanos(RecursosHumanos recursoshumanosToBeUpdated, RecursosHumanos recursoshumanos)
        {
            recursoshumanosToBeUpdated.Id_recursosHumanos = recursoshumanos.Id_recursosHumanos;
            recursoshumanosToBeUpdated.Id_timeSheet = recursoshumanos.Id_timeSheet;

            await _unitOfWork.CommitAsync();
        }

    }
}
