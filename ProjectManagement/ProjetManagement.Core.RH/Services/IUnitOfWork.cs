﻿using ProjectManagement.Core.RH.Repositories;
using System;
using System.Threading.Tasks;


namespace ProjectManagement.Core.RH.Services
{
    public interface IUnitOfWork : IDisposable
    {
        IRecursosHumanosRepository RecursosHumanos { get; }
        ITimeSheetRepository TimeSheets { get; }
        IAtividadeRepository Atividades { get; }
        IProjetoRepository Projetos { get; }
        Task<int> CommitAsync();
    }
}