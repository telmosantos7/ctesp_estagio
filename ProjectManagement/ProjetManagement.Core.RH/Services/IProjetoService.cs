﻿using ProjectManagement.Tables;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManagement.RH.Services
{
    public interface IProjetoService
    {
        Task<IEnumerable<Projeto>> GetAllWithAtividade();
        Task<Projeto> GetAtividadeById(int Id);
        Task<IEnumerable<Projeto>> GetWithAtividadeById_atividade(int Id_atividade);
        Task<Projeto> CreateProjeto(Projeto newProjeto);
        //void UpdateProjeto(Projeto projetoToBeUpdated, Projeto projeto);
        Task DeleteProjeto(Projeto projeto);
        Task<Projeto> GetProjectId(int id);
        Task<Projeto> UpdateProjeto(Projeto up);
    }
}