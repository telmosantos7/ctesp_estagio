﻿using ProjectManagement.Core.RH.Repositories;
using ProjectManagement.RH.Services;
using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.RH.Services
{
   public class AtividadeService : IAtividadeService
    {
        private readonly IUnitOfWork _unitOfWork;
        public AtividadeService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<Atividade> CreateAtividade(Atividade newAtividade)
        {
            await _unitOfWork.Atividades.AddAsync(newAtividade);
            await _unitOfWork.CommitAsync();
            return newAtividade;
        }

        public async Task DeleteAtividade(Atividade atividade)
        {
            _unitOfWork.Atividades.Remove(atividade);
            await _unitOfWork.CommitAsync();
        }

        public async Task<IEnumerable<Atividade>> GetAllWithEsforcoAsync()
        {
            return await _unitOfWork.Atividades
                .GetAllWithEsforcoAsync();
        }
        public async Task<IEnumerable<Atividade>> GetAllWithTimeSheetAsync()
        {
            return await _unitOfWork.Atividades
                .GetAllWithTimeSheetAsync();
        }
        public async Task<Atividade> GetEsforcoById(int Id)
        {
            return await _unitOfWork.Atividades
                .GetWithEsforcoByIdAsync(Id);
        }
        public async Task<Atividade> GetTimeSheetById(int Id)
        {
            return await _unitOfWork.Atividades
                .GetWithTimeSheetByIdAsync(Id);
        }

        public async Task<IEnumerable<Atividade>> GetEsforcoById_esforco(int Id_esforco)
        {
            return await _unitOfWork.Atividades
                .GetAllWithEsforcoById_esforcoAsync(Id_esforco);
        }
            public async Task<IEnumerable<Atividade>> GetTimeSheetById_timeSheet(int Id_timeSheet)
        {
            return await _unitOfWork.Atividades
                .GetAllWithTimeSheetById_timeSheetAsync(Id_timeSheet);
        }

        public async Task UpdateAtividade(Atividade atividadeToBeUpdated, Atividade atividade)
        {
            atividadeToBeUpdated.Id_atividade = atividade.Id_atividade;
            atividadeToBeUpdated.Id_esforco = atividade.Id_esforco;
            atividadeToBeUpdated.Id_timeSheet = atividade.Id_timeSheet;

            await _unitOfWork.CommitAsync();
        }
    }
}
