﻿using ProjectManagement.Core.RH.Repositories;
using ProjectManagement.RH.Services;
using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.RH.Services
{
    public class ProjetoService : IProjetoService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ProjetoService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<Projeto> CreateProjeto(Projeto newProjeto)
        {
            await _unitOfWork.Projetos.AddAsync(newProjeto);
            await _unitOfWork.CommitAsync();
            return newProjeto;
        }

        public async Task DeleteProjeto(Projeto projeto)
        {
            _unitOfWork.Projetos.Remove(projeto);
            await _unitOfWork.CommitAsync();
        }

        public async Task<IEnumerable<Projeto>> GetAllWithAtividade()
        {
            return await _unitOfWork.Projetos
                .GetAllWithAtividadeAsync();
        }

        public async Task<Projeto> GetAtividadeById(int Id)
        {
            return await _unitOfWork.Projetos
                .GetWithAtividadeByIdAsync(Id);
        }

        public async Task<IEnumerable<Projeto>> GetWithAtividadeById_atividade(int Id_atividade)
        {
            return await _unitOfWork.Projetos
                .GetWithAtividadeById_atividadeAsync(Id_atividade);
        }

        public async Task<Projeto> UpdateProjeto(Projeto _Projeto)
        {
            _unitOfWork.Projetos.Update(_Projeto);
            //await _unitOfWork.CommitAsync();
            return _Projeto;
        }



        public async Task<Projeto> GetProjectId(int id)
        {
            return await _unitOfWork.Projetos.GetByIdAsync(id);
        }

    }
}
