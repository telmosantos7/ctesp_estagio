﻿using ProjectManagement.Core.RH.Repositories;
using ProjectManagement.Data.Tables;
using ProjectManagement.RH.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.RH.Services
{
    public class TimeSheetService : ITimeSheetService
    {

        private readonly IUnitOfWork _unitOfWork;
        public TimeSheetService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<TimeSheet> CreateTimeSheet(TimeSheet newTimeSheet)
        {
            await _unitOfWork.TimeSheets.AddAsync(newTimeSheet);
            await _unitOfWork.CommitAsync();
            return newTimeSheet;
        }

        public async Task DeleteTimeSheet(TimeSheet timesheet)
        {
            _unitOfWork.TimeSheets.Remove(timesheet);
            await _unitOfWork.CommitAsync();
        }

        public async Task<IEnumerable<TimeSheet>> GetAllWithUtilizador()
        {
            return await _unitOfWork.TimeSheets
                .GetAllWithUtilizadorAsync();
        }
        public async Task<IEnumerable<TimeSheet>> GetAllWithSubRubrica()
        {
            return await _unitOfWork.TimeSheets
                .GetAllWithSubRubricaAsync();
        }
        public async Task<IEnumerable<TimeSheet>> GetAllWithHistorico()
        {
            return await _unitOfWork.TimeSheets
                .GetAllWithHistoricoAsync();
        }
        public async Task<TimeSheet> GetUtilizadorById(int Id)
        {
            return await _unitOfWork.TimeSheets
                .GetWithUtilizadorByIdAsync(Id);
        }
        public async Task<TimeSheet> GetSubRubricaById(int Id)
        {
            return await _unitOfWork.TimeSheets
                .GetWithSubRubricaByIdAsync(Id);
        }
        public async Task<TimeSheet> GetHistoricoById(int Id)
        {
            return await _unitOfWork.TimeSheets
                .GetWithHistoricoByIdAsync(Id);
        }
        public async Task<IEnumerable<TimeSheet>> GetAllWithUtilizadorById_utilizador(int Id_utilizador)
        {
            return await _unitOfWork.TimeSheets
                .GetAllWithUtilizadorById_utilizadorAsync(Id_utilizador);
        }

        public async Task<IEnumerable<TimeSheet>> GetAllWithSubRubricaById_subRubrica(int Id_subRubrica)
        {
            return await _unitOfWork.TimeSheets
                .GetAllWithSubRubricaById_subRubricaAsync(Id_subRubrica);
        }

        public async Task<IEnumerable<TimeSheet>> GetAllWithHistoricoById_historico(int Id_historico)
        {
            return await _unitOfWork.TimeSheets
                .GetAllWithHistoricoById_historicoAsync(Id_historico);
        }

        public async Task UpdateTimeSheet(TimeSheet timesheetToBeUpdated, TimeSheet timesheet)
        {
            timesheetToBeUpdated.Id_timeSheet = timesheet.Id_timeSheet;
            timesheetToBeUpdated.Id_utilizador = timesheet.Id_utilizador;
            timesheetToBeUpdated.Id_subRubrica = timesheet.Id_subRubrica;
            timesheetToBeUpdated.Id_historico = timesheet.Id_historico;

            await _unitOfWork.CommitAsync();
        } 

    }
}
