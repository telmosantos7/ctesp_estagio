﻿using ProjectManagement.Core.RH.Repositories;
using System.Threading.Tasks;


namespace ProjectManagement.Core.RH.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Context _DbContext;
        private ProjetoRepository _projetoRepository;
        private AtividadeRepository _atividadeRepository;
        private TimeSheetRepository _timesheetRepository;
        private RecursosHumanosRepository _recursoshumanosRepository;

        public UnitOfWork(Context DbContext)
        {
            this._DbContext = DbContext;
        }

        public IProjetoRepository Projetos => _projetoRepository = _projetoRepository ?? new ProjetoRepository(_DbContext);

        public IAtividadeRepository Atividades => _atividadeRepository = _atividadeRepository ?? new AtividadeRepository(_DbContext);

        public ITimeSheetRepository TimeSheets => _timesheetRepository = _timesheetRepository ?? new TimeSheetRepository(_DbContext);

        public IRecursosHumanosRepository RecursosHumanos => _recursoshumanosRepository = _recursoshumanosRepository ?? new RecursosHumanosRepository(_DbContext);


        public async Task<int> CommitAsync()
        {
            return await _DbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _DbContext.Dispose();
        }
    }
}