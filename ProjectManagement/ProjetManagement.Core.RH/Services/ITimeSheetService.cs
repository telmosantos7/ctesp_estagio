﻿using ProjectManagement.Data.Tables;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManagement.RH.Services
{
    public interface ITimeSheetService
    {
        Task<IEnumerable<TimeSheet>> GetAllWithUtilizador();
        Task<IEnumerable<TimeSheet>> GetAllWithSubRubrica();
        Task<IEnumerable<TimeSheet>> GetAllWithHistorico();
        Task<TimeSheet> GetUtilizadorById(int Id);
        Task<TimeSheet> GetSubRubricaById(int Id);
        Task<TimeSheet> GetHistoricoById(int Id);
        Task<IEnumerable<TimeSheet>> GetAllWithUtilizadorById_utilizador(int Id_utilizador);
        Task<IEnumerable<TimeSheet>> GetAllWithSubRubricaById_subRubrica(int Id_subRubrica);
        Task<IEnumerable<TimeSheet>> GetAllWithHistoricoById_historico(int Id_historico);
        Task<TimeSheet> CreateTimeSheet(TimeSheet newTimeSheet);
        Task UpdateTimeSheet(TimeSheet timesheetToBeUpdated, TimeSheet timesheet);
        Task DeleteTimeSheet(TimeSheet timesheet);
    }
}