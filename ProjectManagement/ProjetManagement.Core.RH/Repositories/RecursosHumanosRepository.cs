﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectManagement.Data.Repositories;
using ProjectManagement.Data.Tables;

namespace ProjectManagement.Core.RH.Repositories
{
    public class RecursosHumanosRepository : Repository<RecursosHumanos>, IRecursosHumanosRepository
    {
        public RecursosHumanosRepository(Context DbContext)
            : base(DbContext)
        { }

        public async Task<IEnumerable<RecursosHumanos>> GetAllWithTimeSheetAsync()
        {
            return await DbContext.RecursosHumanos
                .Include(m => m.TimeSheet)
                .ToListAsync();
        }

        public async Task<RecursosHumanos> GetWithTimeSheetByIdAsync(int Id)
        {
            return await DbContext.RecursosHumanos
                .Include(m => m.TimeSheet)
                .SingleOrDefaultAsync(m => m.Id_timeSheet== Id); ;
        }

        public async Task<IEnumerable<RecursosHumanos>> GetAllWithTimeSheetById_Id_timeSheetAsync(int Id_timeSheet)
        {
            return await DbContext.RecursosHumanos
                .Include(m => m.TimeSheet)
                .Where(m => m.Id_timeSheet == Id_timeSheet)
                .ToListAsync();
        }

        private DbContext Context
        {
            get { return Context as DbContext; }
        }
    }
}