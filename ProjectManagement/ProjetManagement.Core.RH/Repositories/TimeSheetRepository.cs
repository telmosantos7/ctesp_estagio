﻿using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectManagement.Data.Repositories;
using ProjectManagement.Data.Tables;

namespace ProjectManagement.Core.RH.Repositories
{
    public class TimeSheetRepository : Repository<TimeSheet>, ITimeSheetRepository
    {
        public TimeSheetRepository(Context DbContext)
            : base(DbContext)
        { }

        public async Task<IEnumerable<TimeSheet>> GetAllWithUtilizadorAsync()
        {
            return await DbContext.TimeSheet
                .Include(m => m.Utilizador)
                .ToListAsync();
        }
        public async Task<IEnumerable<TimeSheet>> GetAllWithSubRubricaAsync()
        {
            return await DbContext.TimeSheet
                .Include(m => m.SubRubrica)
                .ToListAsync();
        }
        public async Task<IEnumerable<TimeSheet>> GetAllWithHistoricoAsync()
        {
            return await DbContext.TimeSheet
                .Include(m => m.Historico)
                .ToListAsync();
        }

        public async Task<TimeSheet> GetWithUtilizadorByIdAsync(int Id)
        {
            return await DbContext.TimeSheet
                .Include(m => m.Utilizador)
                .SingleOrDefaultAsync(m => m.Id_utilizador == Id); ;
        }
        
        public async Task<TimeSheet> GetWithSubRubricaByIdAsync(int Id)
        {
            return await DbContext.TimeSheet
                .Include(m => m.SubRubrica)
                .SingleOrDefaultAsync(m => m.Id_subRubrica == Id); ;
        }
        public async Task<TimeSheet> GetWithHistoricoByIdAsync(int Id)
        {
            return await DbContext.TimeSheet
                .Include(m => m.Historico)
                .SingleOrDefaultAsync(m => m.Id_historico == Id); ;
        }
        public async Task<IEnumerable<TimeSheet>> GetAllWithUtilizadorById_utilizadorAsync(int Id_utilizador)
        {
            return await DbContext.TimeSheet
                .Include(m => m.Utilizador)
                .Where(m => m.Id_utilizador == Id_utilizador)
                .ToListAsync();
        }

        public async Task<IEnumerable<TimeSheet>> GetAllWithSubRubricaById_subRubricaAsync(int Id_subRubrica)
        {
            return await DbContext.TimeSheet
                .Include(m => m.SubRubrica)
                .Where(m => m.Id_subRubrica == Id_subRubrica)
                .ToListAsync();
        }

        public async Task<IEnumerable<TimeSheet>> GetAllWithHistoricoById_historicoAsync(int Id_historico)
        {
            return await DbContext.TimeSheet
                .Include(m => m.Historico)
                .Where(m => m.Id_historico == Id_historico)
                .ToListAsync();
        }

        private DbContext Context
        {
            get { return Context as DbContext; }
        }
    }
}