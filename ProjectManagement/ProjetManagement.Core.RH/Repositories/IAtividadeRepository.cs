﻿using ProjectManagement.Tables;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectManagement.Data.Repositories;

namespace ProjectManagement.Core.RH.Repositories
{
    public interface IAtividadeRepository : IRepository<Atividade>
    {
        Task<IEnumerable<Atividade>> GetAllWithEsforcoAsync();
        Task<IEnumerable<Atividade>> GetAllWithTimeSheetAsync();
        Task<Atividade> GetWithEsforcoByIdAsync(int Id);
        Task<Atividade> GetWithTimeSheetByIdAsync(int Id);
        Task<IEnumerable<Atividade>> GetAllWithEsforcoById_esforcoAsync(int Id_esforco);
        Task<IEnumerable<Atividade>> GetAllWithTimeSheetById_timeSheetAsync(int Id_timeSheet);

    }
}
