﻿using ProjectManagement.Data.Repositories;
using ProjectManagement.Data.Tables;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManagement.Core.RH.Repositories
{
    public interface ITimeSheetRepository : IRepository<TimeSheet>
    {
        Task<IEnumerable<TimeSheet>> GetAllWithUtilizadorAsync();
        Task<IEnumerable<TimeSheet>> GetAllWithSubRubricaAsync();
        Task<IEnumerable<TimeSheet>> GetAllWithHistoricoAsync();

        Task<TimeSheet> GetWithUtilizadorByIdAsync(int Id);
        Task<TimeSheet> GetWithSubRubricaByIdAsync(int Id);
        Task<TimeSheet> GetWithHistoricoByIdAsync(int Id);

        Task<IEnumerable<TimeSheet>> GetAllWithUtilizadorById_utilizadorAsync(int Id_utilizador);
        Task<IEnumerable<TimeSheet>> GetAllWithSubRubricaById_subRubricaAsync(int Id_subRubrica);
        Task<IEnumerable<TimeSheet>> GetAllWithHistoricoById_historicoAsync(int Id_historico);
    }
}