﻿using ProjectManagement.Data.Repositories;
using ProjectManagement.Tables;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManagement.Core.RH.Repositories
{
    public interface IProjetoRepository : IRepository<Projeto>
    {
        Task<IEnumerable<Projeto>> GetAllWithAtividadeAsync();
        Task<Projeto> GetWithAtividadeByIdAsync(int Id);
        Task<IEnumerable<Projeto>> GetWithAtividadeById_atividadeAsync(int Id_atividade);
        void Update(Projeto projeto);
    }
}

