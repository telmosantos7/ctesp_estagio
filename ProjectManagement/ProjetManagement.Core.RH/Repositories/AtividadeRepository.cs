﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectManagement.Data.Repositories;

using ProjectManagement.Tables;

namespace ProjectManagement.Core.RH.Repositories
{
    public class AtividadeRepository : Repository<Atividade>, IAtividadeRepository
    {
        public AtividadeRepository(Context DbContext)
            : base(DbContext)
        { }

        public async Task<IEnumerable<Atividade>> GetAllWithEsforcoAsync()
        {
            return await DbContext.Atividade
                .Include(m => m.Esforco)
                .ToListAsync();
        }
        public async Task<IEnumerable<Atividade>> GetAllWithTimeSheetAsync()
        {
            return await DbContext.Atividade
                .Include(m => m.TimeSheet)
                .ToListAsync();
        }

        public async Task<Atividade> GetWithEsforcoByIdAsync(int Id)
        {
            return await DbContext.Atividade
                .Include(m => m.Esforco)
                .SingleOrDefaultAsync(m => m.Id_esforco == Id); ;
        }
        public async Task<Atividade> GetWithTimeSheetByIdAsync(int Id)
        {
            return await DbContext.Atividade
                .Include(m => m.TimeSheet)
                .SingleOrDefaultAsync(m => m.Id_timeSheet == Id); ;
        }

        public async Task<IEnumerable<Atividade>> GetAllWithEsforcoById_esforcoAsync(int Id_esforco)
        {
            return await DbContext.Atividade
                .Include(m => m.Esforco)
                .Where(m => m.Id_esforco == Id_esforco)
                .ToListAsync();
        }

        public async Task<IEnumerable<Atividade>> GetAllWithTimeSheetById_timeSheetAsync(int Id_timeSheet)
        {
            return await DbContext.Atividade
                .Include(m => m.TimeSheet)
                .Where(m => m.Id_timeSheet == Id_timeSheet)
                .ToListAsync();
        }

        private DbContext Context
        {
            get { return Context as DbContext; }
        }
    }
}