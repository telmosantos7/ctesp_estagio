﻿using ProjectManagement.Data.Repositories;
using ProjectManagement.Data.Tables;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace ProjectManagement.Core.RH.Repositories
{
    public interface IRecursosHumanosRepository : IRepository<RecursosHumanos>
    {
        Task<IEnumerable<RecursosHumanos>> GetAllWithTimeSheetAsync();
        Task<RecursosHumanos> GetWithTimeSheetByIdAsync(int Id);
        Task<IEnumerable<RecursosHumanos>> GetAllWithTimeSheetById_Id_timeSheetAsync(int Id_timeSheet);
    }
}