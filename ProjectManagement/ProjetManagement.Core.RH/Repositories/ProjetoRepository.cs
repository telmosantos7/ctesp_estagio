﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectManagement.Data.Repositories;
using ProjectManagement.Tables;

namespace ProjectManagement.Core.RH.Repositories
{
    public class ProjetoRepository : Repository<Projeto>, IProjetoRepository
    {
        public ProjetoRepository(Context DbContext)
            : base(DbContext)
        { }

        public async Task<IEnumerable<Projeto>> GetAllWithAtividadeAsync()
        {
            return await DbContext.Projeto
                .Include(m => m.Atividade)
                .ToListAsync();
        }

        public async Task<Projeto> GetWithAtividadeByIdAsync(int Id)
        {
            return await DbContext.Projeto
                .Include(m => m.Atividade)
                .SingleOrDefaultAsync(m => m.Id_projeto == Id); ;
        }
        public async Task<IEnumerable<Projeto>> GetWithAtividadeById_atividadeAsync(int Id_atividade)
        {
            return await DbContext.Projeto
                .Include(m => m.Atividade)
                .Where(m => m.Id_atividade == Id_atividade)
                .ToListAsync();
        }

        public void Update(Projeto projeto)
        {
            DbContext.Entry(projeto).State = EntityState.Detached;
            DbContext.Update(projeto);
            DbContext.SaveChanges();
      
        }

        private DbContext Context
        { 
            get { return Context as DbContext; }
        }
    }
}