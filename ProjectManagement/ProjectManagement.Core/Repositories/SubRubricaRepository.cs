﻿using Microsoft.EntityFrameworkCore;
using ProjectManagement.Data.Repositories;
using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Repositories
{
   public class SubRubricaRepository : Repository<SubRubrica>, ISubRubricaRepository
    {
        public SubRubricaRepository(Context DbContext)
            : base( DbContext)
        { }
        public async Task<IEnumerable<SubRubrica>> GetAllWithRubricaAsync()
        {
            return await DbContext.SubRubrica
                .Include(m => m.Rubrica)
                .ToListAsync();
        }
        public async Task<SubRubrica> GetWithRubricaByIdAsync(int Id)
        {
            return await DbContext.SubRubrica
                .Include(m => m.Rubrica)
                .SingleOrDefaultAsync(m => m.Id_rubrica == Id); ;
        }
        public async Task<IEnumerable<SubRubrica>> GetAllWithRubricaId_rubricaAsync(int Id_rubrica)
        {
            return await DbContext.SubRubrica
                .Include(m=> m.Rubrica)
                .Where(m => m.Id_rubrica == Id_rubrica)
                .ToListAsync();
        }
        private DbContext Context
        {
            get { return Context as DbContext; }
        }
    }
}