﻿using ProjectManagement.Data.Repositories;
using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Repositories
{
    public interface ISubRubricaRepository : IRepository<SubRubrica>
    {
        Task<IEnumerable<SubRubrica>> GetAllWithRubricaAsync();
        Task<SubRubrica> GetWithRubricaByIdAsync(int Id);
        Task<IEnumerable<SubRubrica>> GetAllWithRubricaId_rubricaAsync(int Id_rubrica);
    }
}
