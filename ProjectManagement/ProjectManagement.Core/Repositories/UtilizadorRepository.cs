﻿using Microsoft.EntityFrameworkCore;
using ProjectManagement.Data.Repositories;
using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectManagement.Core.Repositories
{
    public class UtilizadorRepository : Repository<Utilizador>, IUtilizadorRepository
    {
        public UtilizadorRepository(Context DbContext)
            : base(DbContext)
        { }
        public void Update(Utilizador utilizador)
        {
            DbContext.Entry(utilizador).State = EntityState.Detached;
            DbContext.Update(utilizador);
            DbContext.SaveChanges();

        }
        private DbContext Context
        {
            get { return Context as DbContext; }
        }

    }
}
