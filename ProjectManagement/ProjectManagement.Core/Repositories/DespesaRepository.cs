﻿using Microsoft.EntityFrameworkCore;
using ProjectManagement.Data.Repositories;
using ProjectManagement.Tables;
using ProjectManagement.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Repositories
{
    public class DespesaRepository : Repository<Despesa>, IDespesaRepository
    {
        public DespesaRepository(Context DbContext)
            : base(DbContext)
        { }
        public async Task<IEnumerable<Despesa>> GetAllWithSubRubricaAsync()
        {
            return await DbContext.Despesa
                .Include(m => m.SubRubrica)
                .ToListAsync();
        }
        public async Task<Despesa> GetWithSubRubricaByIdAsync(int Id)
        {
            return await DbContext.Despesa
                    .Include(m => m.SubRubrica)
                    .SingleOrDefaultAsync(m => m.Id_despesa == Id);
        }
        public async Task<IEnumerable<Despesa>> GetAllWithSubRubricaById_subRubricaAsync(int Id_subRubrica)
        {
            return await DbContext.Despesa
                .Include(m => m.SubRubrica)
                .Where(m => m.Id_subRubrica == Id_subRubrica)
                .ToListAsync();
        }
        public void Update(Despesa despesa)
        {
            DbContext.Entry(despesa).State = EntityState.Detached;
            DbContext.Update(despesa);
            DbContext.SaveChanges();

        }
        private DbContext Context
        {
            get { return Context as DbContext; }
        }
    }
}