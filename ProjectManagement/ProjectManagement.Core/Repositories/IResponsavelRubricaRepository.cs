﻿using ProjectManagement.Data.Repositories;
using ProjectManagement.Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Repositories
{
    public interface IResponsavelRubricaRepository : IRepository<ResponsavelRubrica>
    {
        Task<IEnumerable<ResponsavelRubrica>> GetAllWithUtilizadorAsync();
        Task<IEnumerable<ResponsavelRubrica>> GetAllWithRubricaAsync();
        Task<ResponsavelRubrica> GetWithUtilizadorByIdAsync(int Id);
        Task<ResponsavelRubrica> GetWithRubricaByIdAsync(int Id);
        Task<IEnumerable<ResponsavelRubrica>> GetAllWithUtilizadorId_utilizadorAsync(int Id_utilizador);
        Task<IEnumerable<ResponsavelRubrica>> GetAllWithRubricaById_rubricaAsync(int Id_rubrica);
    }
}
