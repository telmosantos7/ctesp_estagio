﻿using Microsoft.EntityFrameworkCore;
using ProjectManagement.Data.Repositories;
using ProjectManagement.Data.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Repositories
{
   public class ResponsavelRubricaRepository : Repository<ResponsavelRubrica>, IResponsavelRubricaRepository
    {
        public ResponsavelRubricaRepository(Context DbContext)
            : base(DbContext)
        { }
        public async Task<IEnumerable<ResponsavelRubrica>> GetAllWithUtilizadorAsync()
        {
            return await DbContext.ResponsavelRubrica
                .Include(m => m.Utilizador)
                .ToListAsync();
        }
        public async Task<IEnumerable<ResponsavelRubrica>> GetAllWithRubricaAsync()
        {
            return await DbContext.ResponsavelRubrica
                .Include(m => m.Rubrica)
                .ToListAsync();
        }
        public async Task<ResponsavelRubrica> GetWithUtilizadorByIdAsync(int Id)
        {
            return await DbContext.ResponsavelRubrica
                .Include(m => m.Utilizador)
                .SingleOrDefaultAsync(m => m.Id_utilizador == Id); ;
        }
        public async Task<ResponsavelRubrica> GetWithRubricaByIdAsync(int Id)
        {
            return await DbContext.ResponsavelRubrica
                .Include(m => m.Rubrica)
                .SingleOrDefaultAsync(m => m.Id_rubrica == Id); ;
        }
        public async Task<IEnumerable<ResponsavelRubrica>> GetAllWithUtilizadorId_utilizadorAsync(int Id_utilizador)
        {
            return await DbContext.ResponsavelRubrica
                .Include(m => m.Utilizador)
                .Where(m => m.Id_utilizador == Id_utilizador)
                .ToListAsync();
        }
        public async Task<IEnumerable<ResponsavelRubrica>> GetAllWithRubricaById_rubricaAsync(int Id_rubrica)
        {
            return await DbContext.ResponsavelRubrica
            .Include(m => m.Rubrica)
            .Where(m => m.Id_rubrica == Id_rubrica)
            .ToListAsync();
        }
        private DbContext Context
        {
            get { return Context as DbContext;  }
        }
     }
}
