﻿using ProjectManagement.Data.Repositories;
using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectManagement.Core.Repositories
{
    public interface IUtilizadorRepository : IRepository<Utilizador>
    {
        void Update(Utilizador utilizador);
    }
}
