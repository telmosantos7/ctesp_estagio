﻿using ProjectManagement.Data.Repositories;
using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Repositories
{
    public interface IDespesaRepository : IRepository<Despesa>
    {
        Task<IEnumerable<Despesa>> GetAllWithSubRubricaAsync();
        Task<Despesa> GetWithSubRubricaByIdAsync(int Id);
        Task<IEnumerable<Despesa>> GetAllWithSubRubricaById_subRubricaAsync(int Id_subRubrica);
        void Update(Despesa despesa);
    }
}