﻿using ProjectManagement.Tables;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Services
{
    public class DespesaService : IDespesaService
    {
        private readonly IUnitOfWork _unitOfWork;
        public DespesaService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public async Task<Despesa> CreateDespesa(Despesa newDespesa)
        {
            await _unitOfWork.Despesas.AddAsync(newDespesa);
            await _unitOfWork.CommitAsync();
            return newDespesa;
        }
        public async Task DeleteDespesa(Despesa despesa)
        {
            _unitOfWork.Despesas.Remove(despesa);
            await _unitOfWork.CommitAsync();
        }
        public async Task<IEnumerable<Despesa>> GetAllWithSubRubrica()
        {
            return await _unitOfWork.Despesas
                .GetAllWithSubRubricaAsync();
        }
        public async Task<Despesa> GetDespesaById(int id)
        {
            return await _unitOfWork.Despesas
                .GetWithSubRubricaByIdAsync(id);
        }
        public async Task<IEnumerable<Despesa>> GetDespesasById_SubRubrica(int id_SubRubrica)
        {
            return await _unitOfWork.Despesas
                .GetAllWithSubRubricaById_subRubricaAsync(id_SubRubrica);
        }
        public async Task<Despesa> UpdateDespesa(Despesa _Despesa)

        {
            _unitOfWork.Despesas.Update(_Despesa);
            await _unitOfWork.CommitAsync();
            return _Despesa;
        }
    }
}
