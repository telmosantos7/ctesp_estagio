﻿using ProjectManagement.Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Services
{
   public class ResponsavelRubricaService : IResponsavelRubricaService
    {
        private readonly IUnitOfWork _unitOfWork;
            public ResponsavelRubricaService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public async Task<ResponsavelRubrica> CreateResponsavelRubrica(ResponsavelRubrica newResponsavelRubrica)
        {
            await _unitOfWork.ResponsavelRubricas.AddAsync(newResponsavelRubrica);
            await _unitOfWork.CommitAsync();
            return newResponsavelRubrica;
        }
        public async Task DeleteResponsavelRubrica(ResponsavelRubrica responsavelRubrica)
        {
            _unitOfWork.ResponsavelRubricas.Remove(responsavelRubrica);
            await _unitOfWork.CommitAsync();
        }
        public async Task<IEnumerable<ResponsavelRubrica>> GetAllWithUtilizador()
        {
            return await _unitOfWork.ResponsavelRubricas
                .GetAllWithUtilizadorAsync();
        }
        public async Task<IEnumerable<ResponsavelRubrica>> GetAllWithRubrica()
        {
            return await _unitOfWork.ResponsavelRubricas
                .GetAllWithRubricaAsync();
        }
        public async Task<ResponsavelRubrica> GetUtilizadorById(int id)
        {
            return await _unitOfWork.ResponsavelRubricas
                .GetWithUtilizadorByIdAsync(id);
        }
        public async Task<ResponsavelRubrica> GetRubricaById(int id)
        {
            return await _unitOfWork.ResponsavelRubricas
                .GetWithRubricaByIdAsync(id);
        }
        public async Task<IEnumerable<ResponsavelRubrica>> GetResponsavelRubricasById_utilizador(int Id_utilizador)
        {
            return await _unitOfWork.ResponsavelRubricas
                .GetAllWithUtilizadorId_utilizadorAsync(Id_utilizador);
        }
        public async Task<IEnumerable<ResponsavelRubrica>> GetResponsavelRubricasById_rubrica(int Id_rubrica)
        {
            return await _unitOfWork.ResponsavelRubricas
                .GetAllWithRubricaById_rubricaAsync(Id_rubrica);
        }
        public async Task UpdateResponsavelRubrica(ResponsavelRubrica responsavelRubricaToBeUpdated, ResponsavelRubrica responsavelRubrica)
        {
            responsavelRubricaToBeUpdated.Estado = responsavelRubrica.Estado;
            responsavelRubricaToBeUpdated.Id_utilizador = responsavelRubrica.Id_utilizador;
            responsavelRubricaToBeUpdated.Id_rubrica = responsavelRubricaToBeUpdated.Id_rubrica;

            await _unitOfWork.CommitAsync();
        }
    }
}
