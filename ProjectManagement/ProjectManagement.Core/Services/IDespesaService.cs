﻿using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Services
{
    public interface IDespesaService
    {
        Task<IEnumerable<Despesa>> GetAllWithSubRubrica();
        Task<Despesa> GetDespesaById(int Id_despesa);
        Task<IEnumerable<Despesa>> GetDespesasById_SubRubrica(int id_SubRubrica);
        Task<Despesa> CreateDespesa(Despesa newDespesa);
        //Task UpdateDespesa(Despesa despesaToBeUpdated, Despesa despesa);
        Task DeleteDespesa(Despesa Despesa);
        Task<Despesa> UpdateDespesa(Despesa up);
    }
}
