﻿
using ProjectManagement.Core.Repositories;
using System; 
using System.Collections.Generic; 
using System.Text; 
using System.Threading.Tasks;

namespace ProjectManagement.Core.Services
{
        public interface IUnitOfWork : IDisposable
        {
            IDespesaRepository Despesas { get; }
            IResponsavelRubricaRepository ResponsavelRubricas { get; }
            ISubRubricaRepository SubRubricas { get; }
            IUtilizadorRepository Utilizadores { get; }
            Task<int> CommitAsync();
        }
    }


