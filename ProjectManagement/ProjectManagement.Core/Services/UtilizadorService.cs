﻿using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Services
{
    public class UtilizadorService : IUtilizadorService
    {
        private readonly IUnitOfWork _unitOfWork;
        public UtilizadorService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }


        public async Task<Utilizador> CreateUtilizador(Utilizador newUtilizador)
        {
            await _unitOfWork.Utilizadores.AddAsync(newUtilizador);
            await _unitOfWork.CommitAsync();
            return newUtilizador;
        }
        public async Task DeleteUtilizador(Utilizador utilizador)
        {
            _unitOfWork.Utilizadores.Remove(utilizador);
            await _unitOfWork.CommitAsync();
        }
        public async Task<Utilizador> GetUtilizadorById(int id)
        {
            return await _unitOfWork.Utilizadores
                .GetByIdAsync(id);

    }

        public Task<IEnumerable<Utilizador>> GetUtilizadores()
        {
            return _unitOfWork.Utilizadores.GetAllAsync();
        }

        public async Task<Utilizador> UpdateUtilizador(Utilizador _Utilizador)

        {
            _unitOfWork.Utilizadores.Update(_Utilizador);
            await _unitOfWork.CommitAsync();
            return _Utilizador;
        }
    }
}
