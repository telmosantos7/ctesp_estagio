﻿using ProjectManagement.Data.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Services
{
   public interface IResponsavelRubricaService
    {
        Task<IEnumerable<ResponsavelRubrica>> GetAllWithUtilizador();
        Task<IEnumerable<ResponsavelRubrica>> GetAllWithRubrica();
        Task<ResponsavelRubrica> GetUtilizadorById(int id);
        Task<ResponsavelRubrica> GetRubricaById(int id);
        Task<IEnumerable<ResponsavelRubrica>> GetResponsavelRubricasById_utilizador(int Id_utilizador);
        Task<IEnumerable<ResponsavelRubrica>> GetResponsavelRubricasById_rubrica(int Id_rubrica);
        Task<ResponsavelRubrica> CreateResponsavelRubrica(ResponsavelRubrica newResponsavelRubrica);
        Task UpdateResponsavelRubrica(ResponsavelRubrica responsavelRubricaToBeUpdated, ResponsavelRubrica responsavelRubrica);
        Task DeleteResponsavelRubrica(ResponsavelRubrica responsavelRubrica);
    }
}
