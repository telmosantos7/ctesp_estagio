﻿using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Services
{
    public class SubRubricaService : ISubRubricaService
    {
        private readonly IUnitOfWork _unitOfWork;
        public SubRubricaService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public async Task<SubRubrica> CreateSubRubrica(SubRubrica newSubRubrica)
        {
            await _unitOfWork.SubRubricas.AddAsync(newSubRubrica);
            await _unitOfWork.CommitAsync();
            return newSubRubrica;
        }
        public async Task DeleteSubRubrica(SubRubrica subRubrica)
        {
            _unitOfWork.SubRubricas.Remove(subRubrica);
            await _unitOfWork.CommitAsync();
        }
        public async Task<IEnumerable<SubRubrica>> GetAllWithRubrica()
        {
            return await _unitOfWork.SubRubricas
                .GetAllWithRubricaAsync();
        }
        public async Task<SubRubrica> GetRubricaById(int id)
        {
            return await _unitOfWork.SubRubricas
                .GetWithRubricaByIdAsync(id);

        }
        public async Task<IEnumerable<SubRubrica>> GetRubricasById_rubrica(int Id_rubrica)
        {
            return await _unitOfWork.SubRubricas
                .GetAllWithRubricaId_rubricaAsync(Id_rubrica);
        }
        public async Task UpdateSubRubrica(SubRubrica subRubricaToBeUpdated, SubRubrica subRubrica)
        {
            subRubricaToBeUpdated.Descricao_subRubrica = subRubrica.Descricao_subRubrica;
            subRubricaToBeUpdated.Id_rubrica = subRubrica.Id_rubrica;
            await _unitOfWork.CommitAsync();
        }
            }
}
