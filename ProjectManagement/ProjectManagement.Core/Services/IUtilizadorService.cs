﻿using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Services
{
    public interface IUtilizadorService
    {
        Task<Utilizador> GetUtilizadorById(int Id_utilizador);
        Task<Utilizador> CreateUtilizador(Utilizador newUtilizador);
        Task DeleteUtilizador(Utilizador Utilizador);
        Task<Utilizador> UpdateUtilizador(Utilizador up);
        Task<IEnumerable<Utilizador>> GetUtilizadores();
    }
}
