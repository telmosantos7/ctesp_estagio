﻿using Microsoft.EntityFrameworkCore;
using ProjectManagement.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Core.Services
 { 
   public class UnitOfWork : IUnitOfWork
    {
        private readonly Context _DbContext;
         private DespesaRepository _despesaRepository;
         private ResponsavelRubricaRepository _responsavelRubricaRepository;
         private SubRubricaRepository _subRubricaRepository;
        private UtilizadorRepository _utilizadorRepository;

        public UnitOfWork(Context DbContext)
        {
            this._DbContext = DbContext;
        }
          public IDespesaRepository Despesas => _despesaRepository = _despesaRepository ?? new DespesaRepository(_DbContext);
          public IResponsavelRubricaRepository ResponsavelRubricas => _responsavelRubricaRepository = _responsavelRubricaRepository ?? new ResponsavelRubricaRepository(_DbContext);
          public ISubRubricaRepository SubRubricas => _subRubricaRepository = _subRubricaRepository ?? new SubRubricaRepository(_DbContext);
        public IUtilizadorRepository Utilizadores => _utilizadorRepository = _utilizadorRepository ?? new UtilizadorRepository(_DbContext);

        public async Task<int> CommitAsync()
        {
            return await _DbContext.SaveChangesAsync();
        }
        public void Dispose()
        {
            _DbContext.Dispose();
        }
    }
}
