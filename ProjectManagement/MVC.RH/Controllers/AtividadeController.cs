﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MVC.RH.Models.Atividade;

namespace MVC.RH.Controllers
{
    public class AtividadeController : Controller
    {
        public IActionResult Index()
        {
            IEnumerable<ListAtividade> atvlist;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync(GlobalVariables.apiVersion + "/Atividade").Result;
            atvlist = response.Content.ReadAsAsync<IEnumerable<ListAtividade>>().Result;
            return View(atvlist);
        }

        public ActionResult Create()
        {
            return View();

        }

        [HttpPost]
        public ActionResult Create(CreateAtividade atv)
        {
            if (atv.Id_atividade == 0)
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync(GlobalVariables.apiVersion + "/Atividade/", atv).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync(GlobalVariables.apiVersion + "/Atividade/" + id.ToString()).Result;
                    return View(response.Content.ReadAsAsync<EditAtividade>().Result);
                }
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult Edit(EditAtividade eatv)
        {
            if (eatv.Id_atividade != 0)
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync(GlobalVariables.apiVersion + "/Atividade/" + eatv.Id_atividade, eatv).Result;
                if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
                {
                    return BadRequest();
                }
                TempData["SuccessMessage"] = "Updated Successfully";
            }
            return RedirectToAction("Index");
        }

    }
}