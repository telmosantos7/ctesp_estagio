﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MVC.RH.Models.Projeto;
//using ProjectManagement.RH.Services;
//using ProjectManagement.Tables;

namespace MVC.RH.Controllers
{
    public class ProjetoController : Controller
    {

       // String connectionString = @"Server=DESKTOP-QPHNPSI\SQLEXPRESS; Database=ProjectManagementBD;User=rubenterra;Password=123;";
       // private readonly IProjetoService _projetoService;
        //public ProjetoController(IProjetoService projetoService)
        //{
        //    this._projetoService = projetoService;
        //}

        public ActionResult Index()
        {
            //    ListProjetos prj1 = new ListProjetos { Id = 1, Nome = "projetotest",CustoTotal = 100 };
            //    ListProjetos prj2 = new ListProjetos { Id = 2, Nome = "projetotest2",CustoTotal = 200 };
            //    List<ListProjetos> ListPr = new List<ListProjetos>
            //    {
            //        prj1,
            //        prj2
            //    };
            //    return View(ListPr);
            IEnumerable<ListProjetos> prjlist;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync(GlobalVariables.apiVersion + "/Projeto").Result;
            prjlist = response.Content.ReadAsAsync<IEnumerable<ListProjetos>>().Result;
            return View(prjlist);
        }

        //public ActionResult Create(int id = 0)
        //{
        //    if (id == null)
        //    {
        //        HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync(GlobalVariables.apiVersion + "/Despesas/" + id.ToString()).Result;
        //        return View(response.Content.ReadAsAsync<ListProjetos>().Result);
        //    }
        //    {
        //        return BadRequest();
        //    }

        //}

        //[HttpPost]
        //public ActionResult Create(CreateProjeto desp)
        //{
        //    if (desp.Id_projeto == 0)
        //    {
        //        HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync(GlobalVariables.apiVersion + "/Projeto/", desp).Result;
        //        TempData["SuccessMessage"] = "Saved Successfully";
        //    }

        //    return RedirectToAction("Index");
        //}

        public ActionResult Create()
        {
            return View();

        }

        [HttpPost]
        public ActionResult Create(CreateProjeto proj)
        {
            if (proj.Id_projeto == 0)
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync(GlobalVariables.apiVersion + "/Projeto/", proj).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync(GlobalVariables.apiVersion + "/Projeto/" + id.ToString()).Result;
                    return View(response.Content.ReadAsAsync<EditProjeto>().Result);
                }
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult Edit(EditProjeto eproj)
        {
            if (eproj.Id_projeto != 0)
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync(GlobalVariables.apiVersion + "/Projeto/" + eproj.Id_projeto, eproj).Result;
                if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
                {
                    return BadRequest();
                }
                TempData["SuccessMessage"] = "Updated Successfully";
            }
            return RedirectToAction("Index");
        }

        //public ActionResult Edit(int? id)
        //{
        //    if (id != null)
        //    {
        //        HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync(GlobalVariables.apiVersion + "/Projeto/" + id.ToString()).Result;
        //        return View(response.Content.ReadAsAsync<EditProjeto>().Result);
        //    }
        //    {
        //        return BadRequest();
        //    }
        //}

        //[HttpPost]
        //public ActionResult Edit(EditProjeto eproj)
        //{
        //    if (eproj.Id_projeto != 0)
        //    {
        //        HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync(GlobalVariables.apiVersion + "/Projeto/" + eproj.Id_projeto, eproj).Result;
        //        TempData["SuccessMessage"] = "Updated Successfully";
        //    }
        //    return RedirectToAction("Index");
        //}

        public IActionResult Details(int id)
        {
            DetailsProjetos prj1 = new DetailsProjetos { Id_projeto = 1, Nome_projeto = "projetotest", Horas = 0 };

            return View(prj1);
        }






        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Projeto>>> GetAllExpensives()
        //{
        //    try
        //    {
        //        var expensives = await _projetoService.GetAllWithAtividade();
        //        return Ok(expensives);
        //    }
        //    catch (System.Exception ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }



        //}

        //[HttpDelete("{Id_projeto}")]
        //public async Task<ActionResult<IEnumerable<Projeto>>> DeleteProjeto(int Id_projeto)
        //{

        //    if (Id_projeto == 0)
        //        return BadRequest();
        //    var projeto = await _projetoService.GetAtividadeById(Id_projeto);
        //    if (projeto == null)
        //        return NotFound();

        //    await _projetoService.DeleteProjeto(projeto);
        //    return NoContent();

        //}

        //[HttpPost]
        //public async Task<ActionResult<IEnumerable<Projeto>>> CreateProjeto(Projeto newProjeto)
        //{
        //    try
        //    {
        //        var projeto = await _projetoService.CreateProjeto(newProjeto);
        //        var response = await _projetoService.CreateProjeto(projeto);
        //        return Ok(response);

        //    }
        //    catch (System.Exception ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }
        //}


        //[HttpPut]
        //public IActionResult UpdateProjeto(Projeto projetoUp)
        //{
        //    if (projetoUp == null)
        //    {
        //        return BadRequest();
        //    }

        //    //var projetoTobeUpdate = await _projetoService.GetProjectId(projetoUp.Id_projeto);

        //    var response = _projetoService.UpdateProjeto(projetoUp);

        //    if (response == null)
        //    {
        //        return NotFound();
        //    }

        //    return NoContent();
        //}



    }
}
    
