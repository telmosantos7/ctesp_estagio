﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.RH.Models.Atividade
{
    public class ListAtividade
    {
        public int Id_atividade { get; set; }
        public string Id_timeSheet { get; set; }
        public int Id_tarefa { get; set; }
        public int Id_esforco { get; set; }
        public DateTime Data_inicio { get; set; }
        public DateTime Data_fim { get; set; }
        public string Tipo_classificacao { get; set; }
    }
}
