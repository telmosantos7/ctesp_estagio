﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.RH.Models.Atividade
{
    public class DetailsAtividade
    {
        public int Id_atividade { get; set; }
        public string Id_timeSheet { get; set; }
        public int Id_tarefa { get; set; }
        public int Id_esforco { get; set; }
        public int Data_inicio { get; set; }
        public int Data_fim { get; set; }
        public int Tipo_classificacao { get; set; }
    }
}
