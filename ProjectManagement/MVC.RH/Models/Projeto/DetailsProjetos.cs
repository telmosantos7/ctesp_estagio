﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.RH.Models.Projeto
{
    public class DetailsProjetos
    {
        public int Id_projeto { get; set; }
        public string Nome_projeto { get; set; }
        public int Horas { get; set; }
    }
}
