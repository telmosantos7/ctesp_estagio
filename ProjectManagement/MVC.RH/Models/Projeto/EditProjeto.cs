﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.RH.Models.Projeto
{
    public class EditProjeto
    {
        public int Id_projeto { get; set; }
        public string Nome_projeto { get; set; }
        public int Horas { get; set; }
        public int Estado { get; set; }
        public int Id_atividade { get; set; }
    }
}
