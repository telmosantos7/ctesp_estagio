﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MVC.RH
{
    public static class GlobalVariables
    {
        public static HttpClient WebApiClient = new HttpClient();
        public static string apiVersion = "v1";

        static GlobalVariables()
        {
            WebApiClient.BaseAddress = new Uri("https://localhost:44369");
            WebApiClient.DefaultRequestHeaders.Clear();
            WebApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}

