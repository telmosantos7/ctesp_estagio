﻿using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Services
{
    public interface ISubRubricaService
    {
        Task<IEnumerable<SubRubrica>> GetAllWithRubricas();
        Task<SubRubrica> GetRubricaById(int id);
        Task<IEnumerable<SubRubrica>> GetSubRubricasByRubricasId(int Id_rubrica);
        Task<SubRubrica> CreateSubRubrica(SubRubrica newSubRubrica);
        Task UpdateSubRubrica(SubRubrica subRubricaToBeUpdated, SubRubrica subRubrica);
        Task DeleteSubRubrica(SubRubrica subRubrica);
    }
}
