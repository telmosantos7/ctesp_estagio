﻿using ProjectManagement.Data.Tables;
using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Services
{
    public interface IResponsavelRubricaService
    {
        Task<IEnumerable<Utilizador>> GetAllWithUtilizador();
        Task<IEnumerable<Rubrica>> GetAllWithRubricas();
        Task<ResponsavelRubrica> GetResponsavelRubricaById(int Id);
        Task<IEnumerable<ResponsavelRubrica>> GetResponsavelRubricasByUtilizadorId(int Id_utilizador);
        Task<IEnumerable<ResponsavelRubrica>> GetResponsavelRubricasByRubricasId(int Id_rubricas);
        Task<ResponsavelRubrica> CreateResponsavelRubrica(ResponsavelRubrica newResponsavelRubrica);
        Task UpdateResponsavelRubrica(ResponsavelRubrica responsavelRubricaToBeUpdated, ResponsavelRubrica responsavelRubrica);
        Task DeleteResponsavelRubrica(ResponsavelRubrica responsavelRubrica);

    }
}
