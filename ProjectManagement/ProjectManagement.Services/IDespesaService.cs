﻿using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.Services
{
    public interface IDespesaService
    {
        Task<IEnumerable<SubRubrica>> GetSubRubricas();
        Task<SubRubrica> GetSubRubricaById(int Id);
        Task<IEnumerable<Despesa>> GetDespesasBySubRubricaId(int Id_subRubrica);
        Task<Despesa> CreateDespesa(Despesa newDespesa);
        Task UpdateDespesa(Despesa despesaToBeUpdated, Despesa despesa);
        Task DeleteDespesa(Despesa despesa);

    }
}
