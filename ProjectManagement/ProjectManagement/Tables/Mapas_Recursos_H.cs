﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text;

namespace ProjectManagement.Data.Tables
{
   public class Mapas_Recursos_H
    {
        [Key]
        public int Id_mapa { get; set; }
        public string Reg_esforco { get; set; }
        public string Reg_horas_mes { get; set; }
        public string Reg_horas_mes_proj { get; set; }
    }
}
