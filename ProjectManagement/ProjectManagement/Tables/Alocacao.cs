﻿using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProjectManagement.Data.Tables
{
   public class Alocacao
    {
        [Key] 
        public int Id_alocacao { get; set; }
        public int Id_projeto { get; set; }
        public string Perc_afetacao_prev { get; set; }
        public string Custo_mensal { get; set; }

        [ForeignKey("Id_projeto")]
        public Projeto Projeto { get; set; }
    }
}
