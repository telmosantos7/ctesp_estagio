﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectManagement.Data.Tables
{
    public class Historico
    {
        [Key]
        public int Id_historico { get; set; }
        public string Estado { get; set; }
    }
}
