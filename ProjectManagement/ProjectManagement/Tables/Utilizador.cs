﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectManagement.Tables
{
    public class Utilizador
    {
        [Key]
        public int Id_utilizador { get ; set; }
        public string Nome { get; set; }
        public string Cargo { get; set; }
        public string Email { get; set; }


    }
}
