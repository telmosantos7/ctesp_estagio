﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectManagement.Tables
{
    public class Esforco
    {
        [Key]
        public int Id_esforco { get; set; }
        public string Tipo_esforco { get; set; }
    }
}
