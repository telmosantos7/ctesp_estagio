﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProjectManagement.Tables
{
    public class Execucao_Despesa
    {
        [Key]
        public int Id_despesa { get; set; }
        public string Pagamento_despesa { get; set; }
        public string Inf_fatura { get; set; }
        [ForeignKey("Id_despesa")]
        public Despesa Despesa { get; set; }
    }
}
