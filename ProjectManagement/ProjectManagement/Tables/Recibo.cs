﻿using ProjectManagement.Data.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProjectManagement.Tables
{
   public class Recibo
    {
        [Key]
        public int Id_Recibo { get; set; }
        public int Id_subRubrica { get; set; }
        public int Id_tipoRecebimento { get; set; }
        public string Tipo_adiantamento { get; set; }
        public string Descricao { get; set; }
        [ForeignKey("Id_subRubrica")]
        public SubRubrica Subrica { get; set; }
        [ForeignKey("Id_tipoRecebimento")]
        public TipoRecebimento TipoRecebimento { get; set; }
    }

}
