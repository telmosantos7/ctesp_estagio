﻿using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Text;

namespace ProjectManagement.Data.Tables
{
    public class ResponsavelRubrica
    {
        [Key]
        public int Id_responsavel { get; set; }
        public int Id_utilizador { get; set; }
        public int Id_rubrica { get; set; }
        public DateTime Data_inicio { get; set; }
        public DateTime Data_fim { get; set; }
        public string Estado { get; set; }
        [ForeignKey("Id_utilizador")]
        public Utilizador Utilizador { get; set; }
        [ForeignKey("Id_rubrica")]
        public Rubrica Rubrica { get; set; }
    }
}
