﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectManagement.Data.Tables
{
   public class TipoRecebimento
    {
        [Key]
        public int Id_tipoRecebimento { get; set; }
        public string Descricao { get; set; }
        public string Estado { get; set; }
    }
}
