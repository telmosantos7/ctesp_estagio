﻿using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProjectManagement.Data.Tables
{
    public class TimeSheet
    {
        [Key]
        public int Id_timeSheet { get; set; }
        public int Id_utilizador { get; set; }
        public int Id_subRubrica { get; set; }
        public int Num_linhas { get; set; }
        public string Informacao { get; set; }
        public int Id_historico { get; set; }
        [ForeignKey("Id_utilizador")]
        public Utilizador Utilizador { get; set; }
        [ForeignKey("Id_subRubrica")]
        public SubRubrica SubRubrica { get; set; }
        [ForeignKey("Id_historico")]
        public Historico Historico { get; set; }
    }
}
