﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProjectManagement.Tables
{
    public class SubRubrica
    {
        [Key]
        public int Id_subRubrica { get; set; }
        public int Id_rubrica { get; set; }
        public int Codigo_subRubrica { get; set; }
        public string Descricao_subRubrica { get; set; }
        public string Responsavel { get; set; }
        [ForeignKey("Id_rubrica")]
        public Rubrica Rubrica { get; set; }
    }
}
