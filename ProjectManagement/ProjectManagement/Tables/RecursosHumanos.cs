﻿using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProjectManagement.Data.Tables
{
    public class RecursosHumanos
    {
       [Key]
        public int Id_recursosHumanos { get; set; }
        public int Id_timeSheet { get; set; }
        public string Nome { get; set; }
        public string Categoria { get; set; }
        public string Tipo_recurso { get; set; }
        public string Custo_mensal { get; set; }
        public string Perc_afetacao { get; set; }
        public string Registo_trabalho { get; set; }

        [ForeignKey("Id_timeSheet")]
        public TimeSheet TimeSheet { get; set; }
    }
}
