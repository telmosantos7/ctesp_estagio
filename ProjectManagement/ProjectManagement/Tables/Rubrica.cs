﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectManagement.Tables
{
    public class Rubrica
    {
        [Key]
        public int Id_rubrica { get; set; }
        public int Cod_rubrica { get; set; }
        public string Descricao { get; set; }

    }
}
