﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Text;

namespace ProjectManagement.Tables
{
    public class Despesa
    {
        [Key]
        public int Id_despesa { get; set; }
        public int Id_subRubrica { get; set; }
        public string Pedido_despesa { get; set; }
        public string Descricao { get; set; }
        [ForeignKey("Id_subRubrica")]
        public SubRubrica SubRubrica { get; set; }
    }
}

