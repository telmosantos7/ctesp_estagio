﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Text;

namespace ProjectManagement.Tables
{
   public class Projeto
    {
        [Key]
        public int Id_projeto { get; set; }
        public string Nome_projeto { get; set; }
        public int Horas { get; set; }
        public int Estado { get; set; }
        public int Id_atividade { get; set; }
       [ForeignKey("Id_atividade")]
        public Atividade Atividade { get; set; }

    }
}
