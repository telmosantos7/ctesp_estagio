﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProjectManagement.Tables
{
    public class Mapas_Execucao_Fin
    {
        [Key]
        public int Id_mapa { get; set; }
        public string Tipo_orcamento { get; set; }
    }
}
