﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using ProjectManagement.Data.Tables;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ProjectManagement.Tables
{
    public class Atividade
    {
        [Key]

        public int Id_atividade { get; set; }
        public int Id_timeSheet { get; set; }
        public int Id_tarefa { get; set; }
        public int Id_esforco { get; set; }
        public DateTime Data_inicio { get; set; }
        public DateTime Data_fim { get; set; }
        public string Tipo_classificacao { get; set; }
        [ForeignKey("Id_esforco")]
        public Esforco Esforco { get; set; }
        [ForeignKey("Id_timeSheet")]
        public TimeSheet TimeSheet { get; set; }
    }
}
