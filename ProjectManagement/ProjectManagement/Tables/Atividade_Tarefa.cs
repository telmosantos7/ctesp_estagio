﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace ProjectManagement.Tables
{
	public class Atividade_Tarefa
	{ 
		[Key]
		public int Id_atividade { get; set; }
		public int Id_tarefa { get; set; }
		public string Descricao { get; set; }
		[ForeignKey("Id_atividade")]
		public Atividade Atividade { get; set; }
	}
}
