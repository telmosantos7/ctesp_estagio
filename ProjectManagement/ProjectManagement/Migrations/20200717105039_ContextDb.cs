﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectManagement.Data.Migrations
{
    public partial class ContextDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Esforco",
                columns: table => new
                {
                    Id_esforco = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Tipo_esforco = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Esforco", x => x.Id_esforco);
                });

            migrationBuilder.CreateTable(
                name: "Historico",
                columns: table => new
                {
                    Id_historico = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Estado = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Historico", x => x.Id_historico);
                });

            migrationBuilder.CreateTable(
                name: "Mapas_Execucao_Fin",
                columns: table => new
                {
                    Id_mapa = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Tipo_orcamento = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mapas_Execucao_Fin", x => x.Id_mapa);
                });

            migrationBuilder.CreateTable(
                name: "Mapas_Recursos_H",
                columns: table => new
                {
                    Id_mapa = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Reg_esforco = table.Column<string>(nullable: true),
                    Reg_horas_mes = table.Column<string>(nullable: true),
                    Reg_horas_mes_proj = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mapas_Recursos_H", x => x.Id_mapa);
                });

            migrationBuilder.CreateTable(
                name: "Rubrica",
                columns: table => new
                {
                    Id_rubrica = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Cod_rubrica = table.Column<int>(nullable: false),
                    Descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rubrica", x => x.Id_rubrica);
                });

            migrationBuilder.CreateTable(
                name: "TipoRecebimento",
                columns: table => new
                {
                    Id_tipoRecebimento = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descricao = table.Column<string>(nullable: true),
                    Estado = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoRecebimento", x => x.Id_tipoRecebimento);
                });

            migrationBuilder.CreateTable(
                name: "Utilizador",
                columns: table => new
                {
                    Id_utilizador = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome = table.Column<string>(nullable: true),
                    Cargo = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Utilizador", x => x.Id_utilizador);
                });

            migrationBuilder.CreateTable(
                name: "SubRubrica",
                columns: table => new
                {
                    Id_subRubrica = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Id_rubrica = table.Column<int>(nullable: false),
                    Codigo_subRubrica = table.Column<int>(nullable: false),
                    Descricao_subRubrica = table.Column<string>(nullable: true),
                    Responsavel = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubRubrica", x => x.Id_subRubrica);
                    table.ForeignKey(
                        name: "FK_SubRubrica_Rubrica_Id_rubrica",
                        column: x => x.Id_rubrica,
                        principalTable: "Rubrica",
                        principalColumn: "Id_rubrica",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ResponsavelRubrica",
                columns: table => new
                {
                    Id_responsavel = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Id_utilizador = table.Column<int>(nullable: false),
                    Id_rubrica = table.Column<int>(nullable: false),
                    Data_inicio = table.Column<DateTime>(nullable: false),
                    Data_fim = table.Column<DateTime>(nullable: false),
                    Estado = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResponsavelRubrica", x => x.Id_responsavel);
                    table.ForeignKey(
                        name: "FK_ResponsavelRubrica_Rubrica_Id_rubrica",
                        column: x => x.Id_rubrica,
                        principalTable: "Rubrica",
                        principalColumn: "Id_rubrica",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ResponsavelRubrica_Utilizador_Id_utilizador",
                        column: x => x.Id_utilizador,
                        principalTable: "Utilizador",
                        principalColumn: "Id_utilizador",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Despesa",
                columns: table => new
                {
                    Id_despesa = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Id_subRubrica = table.Column<int>(nullable: false),
                    Pedido_despesa = table.Column<string>(nullable: true),
                    Descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Despesa", x => x.Id_despesa);
                    table.ForeignKey(
                        name: "FK_Despesa_SubRubrica_Id_subRubrica",
                        column: x => x.Id_subRubrica,
                        principalTable: "SubRubrica",
                        principalColumn: "Id_subRubrica",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Recibo",
                columns: table => new
                {
                    Id_Recibo = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Id_subRubrica = table.Column<int>(nullable: false),
                    Id_tipoRecebimento = table.Column<int>(nullable: false),
                    Tipo_adiantamento = table.Column<string>(nullable: true),
                    Descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Recibo", x => x.Id_Recibo);
                    table.ForeignKey(
                        name: "FK_Recibo_SubRubrica_Id_subRubrica",
                        column: x => x.Id_subRubrica,
                        principalTable: "SubRubrica",
                        principalColumn: "Id_subRubrica",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Recibo_TipoRecebimento_Id_tipoRecebimento",
                        column: x => x.Id_tipoRecebimento,
                        principalTable: "TipoRecebimento",
                        principalColumn: "Id_tipoRecebimento",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TimeSheet",
                columns: table => new
                {
                    Id_timeSheet = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Id_utilizador = table.Column<int>(nullable: false),
                    Id_subRubrica = table.Column<int>(nullable: false),
                    Num_linhas = table.Column<int>(nullable: false),
                    Informacao = table.Column<string>(nullable: true),
                    Id_historico = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeSheet", x => x.Id_timeSheet);
                    table.ForeignKey(
                        name: "FK_TimeSheet_Historico_Id_historico",
                        column: x => x.Id_historico,
                        principalTable: "Historico",
                        principalColumn: "Id_historico",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TimeSheet_SubRubrica_Id_subRubrica",
                        column: x => x.Id_subRubrica,
                        principalTable: "SubRubrica",
                        principalColumn: "Id_subRubrica",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TimeSheet_Utilizador_Id_utilizador",
                        column: x => x.Id_utilizador,
                        principalTable: "Utilizador",
                        principalColumn: "Id_utilizador",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Execucao_Despesas",
                columns: table => new
                {
                    Id_despesa = table.Column<int>(nullable: false),
                    Pagamento_despesa = table.Column<string>(nullable: true),
                    Inf_fatura = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Execucao_Despesas", x => x.Id_despesa);
                    table.ForeignKey(
                        name: "FK_Execucao_Despesas_Despesa_Id_despesa",
                        column: x => x.Id_despesa,
                        principalTable: "Despesa",
                        principalColumn: "Id_despesa",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Atividade",
                columns: table => new
                {
                    Id_atividade = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Id_timeSheet = table.Column<int>(nullable: false),
                    Id_tarefa = table.Column<int>(nullable: false),
                    Id_esforco = table.Column<int>(nullable: false),
                    Data_inicio = table.Column<DateTime>(nullable: false),
                    Data_fim = table.Column<DateTime>(nullable: false),
                    Tipo_classificacao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Atividade", x => x.Id_atividade);
                    table.ForeignKey(
                        name: "FK_Atividade_Esforco_Id_esforco",
                        column: x => x.Id_esforco,
                        principalTable: "Esforco",
                        principalColumn: "Id_esforco",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Atividade_TimeSheet_Id_timeSheet",
                        column: x => x.Id_timeSheet,
                        principalTable: "TimeSheet",
                        principalColumn: "Id_timeSheet",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RecursosHumanos",
                columns: table => new
                {
                    Id_recursosHumanos = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Id_timeSheet = table.Column<int>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    Categoria = table.Column<string>(nullable: true),
                    Tipo_recurso = table.Column<string>(nullable: true),
                    Custo_mensal = table.Column<string>(nullable: true),
                    Perc_afetacao = table.Column<string>(nullable: true),
                    Registo_trabalho = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RecursosHumanos", x => x.Id_recursosHumanos);
                    table.ForeignKey(
                        name: "FK_RecursosHumanos_TimeSheet_Id_timeSheet",
                        column: x => x.Id_timeSheet,
                        principalTable: "TimeSheet",
                        principalColumn: "Id_timeSheet",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Atividade_Tarefa",
                columns: table => new
                {
                    Id_atividade = table.Column<int>(nullable: false),
                    Id_tarefa = table.Column<int>(nullable: false),
                    Descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Atividade_Tarefa", x => x.Id_atividade);
                    table.ForeignKey(
                        name: "FK_Atividade_Tarefa_Atividade_Id_atividade",
                        column: x => x.Id_atividade,
                        principalTable: "Atividade",
                        principalColumn: "Id_atividade",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Projeto",
                columns: table => new
                {
                    Id_projeto = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nome_projeto = table.Column<string>(nullable: true),
                    Horas = table.Column<int>(nullable: false),
                    Estado = table.Column<int>(nullable: false),
                    Id_atividade = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projeto", x => x.Id_projeto);
                    table.ForeignKey(
                        name: "FK_Projeto_Atividade_Id_atividade",
                        column: x => x.Id_atividade,
                        principalTable: "Atividade",
                        principalColumn: "Id_atividade",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Alocacao",
                columns: table => new
                {
                    Id_alocacao = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Id_projeto = table.Column<int>(nullable: false),
                    Perc_afetacao_prev = table.Column<string>(nullable: true),
                    Custo_mensal = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Alocacao", x => x.Id_alocacao);
                    table.ForeignKey(
                        name: "FK_Alocacao_Projeto_Id_projeto",
                        column: x => x.Id_projeto,
                        principalTable: "Projeto",
                        principalColumn: "Id_projeto",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Alocacao_Id_projeto",
                table: "Alocacao",
                column: "Id_projeto");

            migrationBuilder.CreateIndex(
                name: "IX_Atividade_Id_esforco",
                table: "Atividade",
                column: "Id_esforco");

            migrationBuilder.CreateIndex(
                name: "IX_Atividade_Id_timeSheet",
                table: "Atividade",
                column: "Id_timeSheet");

            migrationBuilder.CreateIndex(
                name: "IX_Despesa_Id_subRubrica",
                table: "Despesa",
                column: "Id_subRubrica");

            migrationBuilder.CreateIndex(
                name: "IX_Projeto_Id_atividade",
                table: "Projeto",
                column: "Id_atividade");

            migrationBuilder.CreateIndex(
                name: "IX_Recibo_Id_subRubrica",
                table: "Recibo",
                column: "Id_subRubrica");

            migrationBuilder.CreateIndex(
                name: "IX_Recibo_Id_tipoRecebimento",
                table: "Recibo",
                column: "Id_tipoRecebimento");

            migrationBuilder.CreateIndex(
                name: "IX_RecursosHumanos_Id_timeSheet",
                table: "RecursosHumanos",
                column: "Id_timeSheet");

            migrationBuilder.CreateIndex(
                name: "IX_ResponsavelRubrica_Id_rubrica",
                table: "ResponsavelRubrica",
                column: "Id_rubrica");

            migrationBuilder.CreateIndex(
                name: "IX_ResponsavelRubrica_Id_utilizador",
                table: "ResponsavelRubrica",
                column: "Id_utilizador");

            migrationBuilder.CreateIndex(
                name: "IX_SubRubrica_Id_rubrica",
                table: "SubRubrica",
                column: "Id_rubrica");

            migrationBuilder.CreateIndex(
                name: "IX_TimeSheet_Id_historico",
                table: "TimeSheet",
                column: "Id_historico");

            migrationBuilder.CreateIndex(
                name: "IX_TimeSheet_Id_subRubrica",
                table: "TimeSheet",
                column: "Id_subRubrica");

            migrationBuilder.CreateIndex(
                name: "IX_TimeSheet_Id_utilizador",
                table: "TimeSheet",
                column: "Id_utilizador");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Alocacao");

            migrationBuilder.DropTable(
                name: "Atividade_Tarefa");

            migrationBuilder.DropTable(
                name: "Execucao_Despesas");

            migrationBuilder.DropTable(
                name: "Mapas_Execucao_Fin");

            migrationBuilder.DropTable(
                name: "Mapas_Recursos_H");

            migrationBuilder.DropTable(
                name: "Recibo");

            migrationBuilder.DropTable(
                name: "RecursosHumanos");

            migrationBuilder.DropTable(
                name: "ResponsavelRubrica");

            migrationBuilder.DropTable(
                name: "Projeto");

            migrationBuilder.DropTable(
                name: "Despesa");

            migrationBuilder.DropTable(
                name: "TipoRecebimento");

            migrationBuilder.DropTable(
                name: "Atividade");

            migrationBuilder.DropTable(
                name: "Esforco");

            migrationBuilder.DropTable(
                name: "TimeSheet");

            migrationBuilder.DropTable(
                name: "Historico");

            migrationBuilder.DropTable(
                name: "SubRubrica");

            migrationBuilder.DropTable(
                name: "Utilizador");

            migrationBuilder.DropTable(
                name: "Rubrica");
        }
    }
}
