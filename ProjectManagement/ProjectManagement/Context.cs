﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ProjectManagement.Data.Tables;
using ProjectManagement.Tables;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Dynamic;
using System.Security.Cryptography;

namespace ProjectManagement
{
    public class Context : DbContext
    {
        public Context()
        {

        }

        public DbSet<Utilizador> Utilizador { get; set; }
        public DbSet<Rubrica> Rubrica { get; set; }
        public DbSet<Atividade> Atividade { get; set; }
        public DbSet<Alocacao> Alocacao { get; set; }
        public DbSet<Atividade_Tarefa> Atividade_Tarefa { get; set; }
        public DbSet<Despesa> Despesa { get; set; }
        public DbSet<Esforco> Esforco { get; set; }
        public DbSet<Execucao_Despesa> Execucao_Despesas { get; set; }
        public DbSet<Mapas_Execucao_Fin> Mapas_Execucao_Fin { get; set; }
        public DbSet<Mapas_Recursos_H> Mapas_Recursos_H { get; set; }
        public DbSet<Projeto> Projeto { get; set; }
        public DbSet<Recibo> Recibo { get; set; }
        public DbSet<RecursosHumanos> RecursosHumanos { get; set; }
        public DbSet<SubRubrica> SubRubrica { get; set; }
        public DbSet<ResponsavelRubrica> ResponsavelRubrica { get; set; }
        public DbSet<TipoRecebimento> TipoRecebimento { get; set; }
        public DbSet<TimeSheet> TimeSheet { get; set; }
        public DbSet<Historico> Historico { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=DESKTOP-QPHNPSI\SQLEXPRESS; Database=ProjectManagementBD;User=rubenterra;Password=123;");
        }

    }
}

