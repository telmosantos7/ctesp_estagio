﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectManagement.RH.Services;
using ProjectManagement.Tables;

namespace ProjectManagement.RH.API.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    
        public class AtividadeController : ControllerBase
        {

            private readonly IAtividadeService _atividadeService;
            public AtividadeController(IAtividadeService atividadeService)
            {
                this._atividadeService = atividadeService;
            }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Tables.Atividade>>> GetAllExpensives()
        {
            try
            {
                var expensives = await _atividadeService.GetAllWithTimeSheetAsync();
                return Ok(expensives);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }



            [HttpGet("{id}")]
            public async Task<ActionResult<Atividade>> GetById(int id)
            {
                try
                {
                    var expensives = await _atividadeService.GetTimeSheetById(id);
                    return Ok(expensives);
                }
                catch (System.Exception ex)
                {
                    return BadRequest(ex.Message);
                }

            }

            [HttpDelete("{Id_atividade}")]
            public async Task<ActionResult<IEnumerable<Tables.Atividade>>> DeleteAtividade(int Id_atividade)
            {

                if (Id_atividade == 0)
                    return BadRequest();
                var atividade = await _atividadeService.GetTimeSheetById(Id_atividade);
                if (atividade == null)
                    return NotFound();

                await _atividadeService.DeleteAtividade(atividade);
                return NoContent();

            }

            [HttpPost]
            public async Task<ActionResult<IEnumerable<Tables.Atividade>>> CreateAtividade(Atividade newAtividade)
            {
                try
                {
                    var atividade = await _atividadeService.CreateAtividade(newAtividade);
                    var response = await _atividadeService.CreateAtividade(atividade);
                    return Ok(response);

                }
                catch (System.Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            //[HttpPut]
            //public IActionResult UpdateAtividade(Atividade atividadeUp)
            //{
            //    if (atividadeUp == null)
            //    {
            //        return BadRequest();
            //    }

            //    //var projetoTobeUpdate = await _projetoService.GetProjectId(projetoUp.Id_projeto);

            //    var response = _atividadeService.UpdateAtividade(atividadeUp);

            //    if (response == null)
            //    {
            //        return NotFound();
            //    }

            //    return NoContent();
            //}
        }
}