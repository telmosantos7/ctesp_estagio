﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectManagement.Core.RH.Repositories;
using ProjectManagement.RH.Services;
using ProjectManagement.Tables;

namespace ProjectManagement.RH.API.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class ProjetoController : ControllerBase
    {

        private readonly IProjetoService _projetoService;
        public ProjetoController(IProjetoService projetoService)
        {
            this._projetoService = projetoService;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Tables.Projeto>>> GetAllExpensives()
        {
            try
            {
                var expensives = await _projetoService.GetAllWithAtividade();
                return Ok(expensives);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }



        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Projeto>> GetById(int id)
        {
            try
            {
                var expensives = await _projetoService.GetProjectId(id);
                return Ok(expensives);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }



        }

        [HttpDelete("{Id_projeto}")]
        public async Task<ActionResult<IEnumerable<Tables.Projeto>>> DeleteProjeto(int Id_projeto)
        {

            if (Id_projeto == 0)
                return BadRequest();
            var projeto = await _projetoService.GetAtividadeById(Id_projeto);
            if (projeto == null)
                return NotFound();

            await _projetoService.DeleteProjeto(projeto);
            return NoContent();

        }

        [HttpPost]
        public async Task<ActionResult<IEnumerable<Tables.Projeto>>> CreateProjeto(Projeto newProjeto)
        {
            try
            {
                var projeto = await _projetoService.CreateProjeto(newProjeto);
                var response = await _projetoService.CreateProjeto(projeto);
                return Ok(response);

            }
            catch(System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /* [HttpPut("{Id_projeto}")]
         public async Task<ActionResult<IEnumerable<Tables.Projeto>>> UpdateProjeto(Projeto projetoToBeUpdated)
         {
             try
             {
                 var projetoToBeUpdate = await _projetoService.UpdateProjeto(projetoToBeUpdated);
                if (projetoToBeUpdate == null)
                 {
                     return NotFound();
                 }

                 var projeto = await _projetoService.UpdateProjeto(projetoToBeUpdated);
                 await _projetoService.UpdateProjeto(projetoToBeUpdate, projeto);
             }
             catch (System.Exception ex)
             {
                 return BadRequest(ex.Message);
             }
         } */

        [HttpPut]
        public IActionResult UpdateProjeto(Projeto projetoUp)
        {
            if (projetoUp == null)
            {
                return BadRequest();
            }

            //var projetoTobeUpdate = await _projetoService.GetProjectId(projetoUp.Id_projeto);

            var response = _projetoService.UpdateProjeto(projetoUp);

            if (response == null)
            {
                return NotFound();
            }

            return NoContent();
        }


        /*  [HttpPost("{Id_projeto}")]
           public async Task<ActionResult<IEnumerable<Tables.Projeto>>> UpdateProjeto(int Estado, Projeto projeto)
            {
               if (Estado != projeto.Estado)
               {
                   return BadRequest();
            }
               try
             {
                 Projeto originalProjeto = await _projetoService(x => x.Estado == Estado);
             }

            var projeto = new Projeto() { Id_projeto = Id_projeto, Estado = 0 };
             var entry = _projetoService.projetoToBeUpdated(projeto);
            entry.Property(e => e.Estado).IsModified = true; 
            }
            */
    }
}