﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Financial.Models.Utilizador
{
    public class CreateUtilizador
    {
        public int Id_utilizador { get; set; }
        public string Nome { get; set; }
        public string Cargo { get; set; }
        public string Email { get; set; }
    }
}
