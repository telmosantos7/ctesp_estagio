﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Financial.Models.Despesa
{
    public class EditDespesa
    {
        public int id_despesa { get; set; }
        public string descricao { get; set; }
    }
}
