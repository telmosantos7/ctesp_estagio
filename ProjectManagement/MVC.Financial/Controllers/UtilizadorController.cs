﻿using Microsoft.AspNetCore.Mvc;
using MVC.Financial.Models.Utilizador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MVC.Financial.Controllers
{
    public class UtilizadorController : Controller
    {
        public ActionResult Index()
        {
            IEnumerable<ListUtilizador> empList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync(GlobalVariables.apiVersion + "/Utilizador").Result;
            empList = response.Content.ReadAsAsync<IEnumerable<ListUtilizador>>().Result;
            return View(empList);

        }


        public ActionResult Create()
        {
            return View();

        }

        [HttpPost]
        public ActionResult Create(CreateUtilizador util)
        {
            if (util.Id_utilizador == 0)
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync(GlobalVariables.apiVersion + "/Utilizador/", util).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync(GlobalVariables.apiVersion + "/Utilizador/" + id.ToString()).Result;
                    return View(response.Content.ReadAsAsync<EditUtilizador>().Result);
                }
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult Edit(EditUtilizador util)
        {
            if (util.Id_utilizador != 0)
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync(GlobalVariables.apiVersion + "/Utilizador/" + util.Id_utilizador, util).Result;
                if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
                {
                    return BadRequest();
                }
                TempData["SuccessMessage"] = "Updated Successfully";
            }
            return RedirectToAction("Index");
        }
    }
}
