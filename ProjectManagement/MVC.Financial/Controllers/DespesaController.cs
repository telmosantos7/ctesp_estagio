﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MVC.Financial.Models.Despesa;
using ProjectManagement.Core.Services;
using ProjectManagement.Tables;

namespace MVC.Financial.Controllers
{
    public class DespesaController : Controller
    {
        public ActionResult Index()
        {
            IEnumerable<ListDespesa> empList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync(GlobalVariables.apiVersion + "/Despesas").Result;
            empList = response.Content.ReadAsAsync<IEnumerable<ListDespesa>>().Result;
            return View(empList);

        }


        public ActionResult Create()
        {
            return  View();

        }

        [HttpPost]
        public ActionResult Create(CreateDespesa desp)
        {
            if (desp.id_despesa == 0)
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync(GlobalVariables.apiVersion + "/Despesas/", desp).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            try
            {
                if (id != null)
                {
                    HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync(GlobalVariables.apiVersion + "/Despesas/" + id.ToString()).Result;
                    return View(response.Content.ReadAsAsync<EditDespesa>().Result);
                }
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult Edit(EditDespesa edesp)
        {
            if (edesp.id_despesa != 0)
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync(GlobalVariables.apiVersion + "/Despesas/" + edesp.id_despesa, edesp).Result;
                if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
                {
                    return BadRequest();
                }
                TempData["SuccessMessage"] = "Updated Successfully";
            }
            return RedirectToAction("Index");
        }

        //public IActionResult Details(int id)
        //{
        //    //DetailsDespesa des1 = new DetailsDespesa { Id = 1, Nome = "despteste", Descricao = "teste" };

        //    //return View(des1);
        //}

    }
}

